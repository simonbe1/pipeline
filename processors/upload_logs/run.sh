#!/usr/bin/env bash

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

minio="${script_dir}"/../minio/run.sh

mode=""
logdir=""
bucket=""
path=""
while [ "$#" -gt 0 ]; do
    case "$1" in
    --clean) mode=clean ;; # not used, but protocol
    --build) mode=build ;; # not used, but protocol
    --logdir) logdir="${2}"; shift;;
    --bucket) bucket="${2}"; shift;;
    --path)   path="${2}"; shift;;
    --)      shift; break;;
    *)       echo "**** unknown opt: $1" >&1; exit 1;;
    esac
    shift
done

test -z "${logdir}" && exit

find "${logdir}/" -type f | while IFS= read file; do
    if [ -s "$file" ]; then
        $minio --upload --file "${path}"/"$(basename ${file})" \
               --bucket pipeline < "${file}" >/dev/null
    fi
done
