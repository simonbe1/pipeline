#!/usr/bin/env bash

# Docker image name to build and run
docker_img=joblinks/chat-notify

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mode=""
while [ "$#" -gt 0 ]; do
    case "$1" in
    --clean) mode=clean ;;
    --build) mode=build ;;
    --)      shift; break;;
    *)       echo "**** unknown opt: $1" >&1; exit 1;;
    esac
    shift
done


# Build ourselves
if [ "$mode" = "build" ]; then
    exec docker build --no-cache -f "${script_dir}"/Dockerfile -t "${docker_img}" "${script_dir}"
fi


# Remove docker image
if [ "$mode" = "clean" ]; then
    docker image inspect "${docker_img}" 1>/dev/null 2>/dev/null &&\
        docker image rm "${docker_img}"
    exit
fi


# Verify that the image has been built
docker image inspect "${docker_img}" 1>/dev/null 2>/dev/null ||\
    { echo "**** Build docker image ${docker_img} first (run $0 --build)">&2; exit 1; }


# mattersend conf file
conf="${script_dir}"/../../secrets/mattermost/mattersend.conf


# start matterclient
( echo '```'
  echo '-----------------'
  cat -
  echo '-----------------'
  echo '```'
  echo -e 'testlink: [output/logs](https://pipeline.arbetsformedlingen.se/'"$(date +%Y-%m-%d)"'/) (requires VPN for now)'
) | docker run --mount type=bind,source="${conf}",target=/mattersend.conf --rm -i ${docker_img} \
           mattersend --config /mattersend.conf $@
