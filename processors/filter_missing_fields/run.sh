#!/usr/bin/env bash
set -eEu -o pipefail

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Docker image name to build and run
docker_img=joblinks/filter-remove-ads-without-occupationplace

mode=""
filterfile=""
filter=""
while [ "$#" -gt 0 ]; do
    case "$1" in
    --filterfile) filterfile="${2}"; shift;;
    --filter)     filter="${2}"; shift;;
    --clean) mode=clean ;;
    --build) mode=build ;;
    --)      shift; break;;
    *)       echo "**** unknown opt: $1" >&1; exit 1;;
    esac
    shift
done


# Build ourselves
if [ "$mode" = "build" ]; then
    exec docker build --no-cache -f "${script_dir}"/Dockerfile -t "${docker_img}" "${script_dir}"
fi


# Remove docker image
if [ "$mode" = "clean" ]; then
    docker image inspect "${docker_img}" 1>/dev/null 2>/dev/null &&\
        docker image rm "${docker_img}"
    exit
fi

if [ -z "${filterfile}" ]; then
    echo "**** supply --filterfile" >&2; exit 1;
fi

touch "${filterfile}"
docker run --rm -v "${filterfile}":"${filterfile}":Z -i "${docker_img}" bash /usr/local/bin/filter.sh "${filter}" "${filterfile}"
