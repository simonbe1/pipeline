#!/bin/bash

inputdata=$(mktemp)

cat - > "${inputdata}"

nr_ads=$(jq -s '. | length' < "${inputdata}")

echo -e "Pipeline host:\n\t\t$(hostname -f)"
echo
echo -e "Number of ads:\n\t\t${nr_ads}"
echo
echo -ne "Sum of job openings (field .originalJobPosting.totalJobOpenings):\n\t\t"
jq  '. | [ .originalJobPosting.totalJobOpenings ] ' < "${inputdata}" | awk '{sum+=$0} END{print sum}'
echo
echo "Top 20 ad sites (by ads, field .originalJobPosting.url):"
jq -r '. | .originalJobPosting.url' < "${inputdata}" \
    | sed -E 's|^(.*//[^/]+).*$|\1|' \
    | sort | uniq -c | sort -nr | head -n 20 \
    | sed 's|^|\t|'
echo
echo "Top 20 employers (by ads, field .originalJobPosting.hiringOrganization.name):"
jq -r '. | .originalJobPosting.hiringOrganization.name' < "${inputdata}" \
    | sort | uniq -c | sort -nr | head -n 20 \
    | sed 's|^|\t|'
echo
echo "Top 20 places (field .ort[].term):"
jq -r ' . | .ort[].term ' < "${inputdata}" \
    | sed -E 's|^(.*//[^/]+).*$|\1|' \
    | sort | uniq -c | sort -nr \
    | head -n 20 | sed 's|^|\t|'
echo
echo "Ads by scrapers (field .originalJobPosting.scraper):"
jq -r ' . | .originalJobPosting.scraper ' < "${inputdata}" \
    | sed -E 's|^(.*//[^/]+).*$|\1|' \
    | sort | uniq -c | sort -nr | sed 's|^|\t|'
echo

echo "Field presence:"
perl field_presence.pl < "${inputdata}"
echo

function desc_quartile() {
    local percentile="${1}"
    jq  '. | .originalJobPosting.description | length' | sort -n | awk '{all[NR] = $0} END{print all[int(NR*'"${percentile}"' - 0.5)]}'
}

echo "Ad description length distribution in characters (quartiles, field .originalJobPosting.description):"
echo -ne "\t\tQ1: " ; desc_quartile 0.25 < "${inputdata}"
echo -ne "\t\tQ2: " ; desc_quartile 0.5  < "${inputdata}"
echo -ne "\t\tQ3: " ; desc_quartile 0.75 < "${inputdata}"
echo

echo -e "Pipeline version:\n\t${GITCOMMIT}"
echo
