#!/usr/bin/env bash

# Docker image name to build and run
docker_img=joblinks/minio

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


bucket=""
mode=""
file=""
while [ "$#" -gt 0 ]; do
    case "$1" in
    --clean) mode=clean ;;
    --build) mode=build ;;
    --upload) mode=upload ;;
    --download) mode=download ;;
    --ls)    mode=ls ;;
    --bucket)  bucket="$2"; shift ;;
    --file)  file="$2"; shift ;;
    --)      shift; break;;
    *)       echo "**** unknown opt: $1" >&1; exit 1;;
    esac
    shift
done


# Build and exit
if [ "$mode" = "build" ]; then
    test -d data && { echo "**** first remove data/ directory">&2; exit 1; }
    exec docker build --no-cache -f "${script_dir}"/Dockerfile -t "${docker_img}" "${script_dir}"
fi

# Clean and exit
if [ "$mode" = "clean" ]; then
    docker image inspect "${docker_img}" 1>/dev/null 2>/dev/null &&\
        docker image rm "${docker_img}"
    exit
fi

# Required params from here
if [ -z "$file" ]; then echo "**** supply file" >&2; exit 1; fi
if [ -z "$bucket" ]; then echo "**** supply bucket" >&2; exit 1; fi

# Set mc-config dir
mcconfig="${script_dir}"/../../secrets/dot_mc

# Upload/download from stdin to minio file and exit. Parameters: --upload bucketconfig target
if [ -n "$bucket" ] && [ -n "$file" ]; then
    test -f "${mcconfig}"/config.json || { echo "**** configure mc first">&2; exit 1; }

    extraflags=""
    if docker --version|grep -q "^Docker"; then
        extraflags="--user $(id -u):$(id -g)"
    fi

    cmd="docker run ${extraflags} -v ${mcconfig}:/var/tmp/.mc:z --rm --env HOME=/var/tmp -i ${docker_img} /bin/bash -c"

    case "$mode" in
        "upload")   tee >($cmd "/usr/local/bin/mc --insecure pipe ${bucket}/${bucket}/$file");;
        "download")       $cmd "/usr/local/bin/mc --insecure cat  ${bucket}/${bucket}/$file" ;;
        "ls")             $cmd "/usr/local/bin/mc --insecure ls   ${bucket}/${bucket}/$file" ;;
        *) echo "**** bad arg $1" >&1; exit 1 ;;
    esac
fi
