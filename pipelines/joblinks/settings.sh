# if you rely on already set variables below, start by verifying they are set
[[ -n "$proj_root" ]] || { echo "**** missing setting 'proj_root'"; exit 1; }
[[ -n "$USER" ]]      || { echo "**** missing setting 'USER'";      exit 1; }
[[ -n "$test_mode" ]] || { echo "**** missing setting 'test_mode'"; exit 1; }

# required setting ldir, local output directory
ldir=/tmp/pipeline/"${USER}"

# only generate the timestamp once, for consistency in the output file names
today=$(date +%Y-%m-%d)

# Minio base folder for uploads
minio_base="pipeline/$today"

# Upload the scraped file
public_input_file="aggregator/$today.json"

# Add user name to buckets if in test mode, to not overwrite the "real" bucket
if [ "${test_mode}" = 1 ]; then
    minio_base="pipeline-test/${USER}/$today"
    public_input_file="aggregator-test/${USER}/$today.json"
fi

# Minio targets for the pipeline input and output
output_file="${minio_base}/output.json" # the output from the final pipeline processor
input_file="${minio_base}/input.json"   # pipeline input is the scraped, aggregated file
log_dir="${minio_base}/logs"



# Convenient aliases for the pipeline processors
scraping="${proj_root}/processors/scraping/run.sh"
minio="${proj_root}/processors/minio/run.sh"
json_to_jsonl="${proj_root}/processors/json-to-jsonl/run.sh"
job_ad_hash="${proj_root}/processors/job_ad_hash/run.sh"
add_id="${proj_root}/processors/add-id/run.sh"
add_occupation_and_place="${proj_root}/processors/add-occupation-and-place/run.sh"
filter_missing_fields="${proj_root}/processors/filter_missing_fields/run.sh"
add_municipality_from_place="${proj_root}/processors/add-municipality-from-place/run.sh"
mkreport="${proj_root}/processors/make-nightly-report/run.sh"
chat_notify="${proj_root}/processors/chat-notify/run.sh"
upload_logs="${proj_root}/processors/upload_logs/run.sh"
