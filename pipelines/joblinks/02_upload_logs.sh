self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${self_dir}/settings.sh"

pipeoutput="${ldir}/minio2.out"
procrep="${ldir}/pipeline.report"

# required setting pipeline, defining the full pipeline
pipeline=(
    # label                      # command line
    "upload_step_log             $upload_logs --bucket pipeline --path ${log_dir} --logdir ${ldir}"
    "upload_main_log             $upload_logs --bucket pipeline --path ${log_dir}" ## --arg upload_main_log "logdir /var/log/pipeline"
)
