self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${self_dir}/settings.sh"

pipeinput="${ldir}/add_municipality_from_place.out"

# required setting pipeline, defining the full pipeline
pipeline=(
    # label                      # command line
    "source                      cat ${pipeinput}"
    "minio2                      $minio --upload --file '$output_file' --bucket pipeline >/dev/null"
)
