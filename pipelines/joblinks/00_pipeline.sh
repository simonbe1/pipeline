# You need to at least define these variables in a pipeline configuration:
#
#   pipeline - a bash array of strings, describing pipeline processors.


self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${self_dir}/settings.sh"

# flag that we want this pipeline to have a log with the used
# commandline, version and timing
create_report=1

# required setting pipeline, defining the full pipeline
pipeline=(
    # label                      # command line
    "scraping                    $scraping --bucket scrapinghub"
    "trunc                       trunc"
    "json_to_jsonl               $json_to_jsonl"
    "add_id                      $add_id --bucket pipeline --dbfile $(dirname ${minio_base})/db.csv --session-dbfile ${minio_base}/db.csv"
    "filter_missing0             $filter_missing_fields --filterfile ${ldir}/add_id.removed --filter '.id,.originalJobPosting.description,.originalJobPosting.title'"
    "add_occupation_and_place    $add_occupation_and_place"
    "filter_missing1             $filter_missing_fields --filterfile ${ldir}/add_occupation_and_place.removed --filter '.yrke,.ort'"
    "job_ad_hash                 $job_ad_hash"
    "add_municipality_from_place $add_municipality_from_place"
    "terminate                   cat - >/dev/null"
)
