self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${self_dir}/settings.sh"

pipeoutput="${ldir}/add_municipality_from_place.out"
procrep="${ldir}/pipeline.report"

# required setting pipeline, defining the full pipeline
pipeline=(
    # label                      # command line
    "get_pipeline_output         cat $pipeoutput"
    "mkreport                    $mkreport"
    "joinreport                  cat $procrep -"
    "chat_notify                 $chat_notify >/dev/null" # last step, can discard output
)
