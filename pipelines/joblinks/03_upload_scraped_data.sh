self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${self_dir}/settings.sh"

pipeinput="${ldir}/trunc.out"

# required setting pipeline, defining the full pipeline
pipeline=(
    # label                      # command line
    "source                      cat ${pipeinput}"
    "minio0                      $minio --upload --file '$input_file' --bucket pipeline"
    "minio1                      $minio --upload --file '$public_input_file' --bucket scrapinghub >/dev/null"
)
