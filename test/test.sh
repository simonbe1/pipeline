#!/bin/bash
#
# Supply the name of one of the tests as argument.
#
set -eEu -o pipefail

proj_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"/..

# name of a host where to run tests
ssh_test_host=${ssh_test_host:-build}

# branch to test
branch=develop

# pipeline test flags
conf="pipelines/joblinks/00_pipeline.sh"
#conf="pipelines/joblinks/01_report.sh"
#conf="pipelines/joblinks/02_upload_logs.sh"
conf_rundir="pipelines/joblinks"
args="-b $branch --all --test -l 2 --arg scraping --light"

pipeline_args_rundir="$conf_rundir $args"
pipeline_args="$args -c $conf"


function clear_all_images() {
    docker container prune -f
    docker image prune -f

    pushd "${proj_dir}"/processors >/dev/null
    tags=$(echo $(ls -1) | tr ' ' '|')
    names="pipeline|joblinks/add_id|joblinks/add-municipality-from-place|joblinks/add-occupation-and-place|joblinks/chat-notify|joblinks/hashsum|joblinks/json2jsonl|joblinks/minio|shcrawler"
    docker images | sed -E 's|[[:space:]]+| |' | cut -f 1-2 -d' ' \
        | sort | uniq | grep -E "($tags|$names)" | tr ' ' ':'  \
        | xargs --no-run-if-empty docker image rm || true
    popd >/dev/null
}

## Tests, ordered roughly by compositional dependency, from basic to composite

# 0. run run-confdir.sh locally, w/o checkout or build
function local0() {
    cd "${proj_dir}" || exit 1
    time bash -c ". secrets/gitlab-deploy/secrets.sh && bin/run-confdir.sh $pipeline_args_rundir" || exit 1
}

# 1. run deploy.sh locally
function local1() {
    cd "${proj_dir}" || exit 1
    time bash -c ". secrets/gitlab-deploy/secrets.sh && bin/deploy.sh -C $pipeline_args_rundir"
}


# 2. run deploy_on_ubuntu.sh on remote server
function ssh0() {
    cd "${proj_dir}"/test || exit 1
    bash deploy_on_ubuntu.sh --connect "ssh ${ssh_test_host}" "-C $pipeline_args_rundir"
}


# 3. run on a clean machine
function vagrant0() {
    cd "${proj_dir}"/test || exit 1
    if LANG=C vagrant status|grep running; then
        vagrant destroy -f
    fi
    vagrant up
    bash deploy_on_ubuntu.sh --connect "vagrant ssh" "-C ${pipeline_args_rundir}"
}

# run a second time on a machine where the images have already been built
function vagrant1() {
    vagrant0
    cd "${proj_dir}"/test || exit 1
    bash deploy_on_ubuntu.sh --connect "vagrant ssh" ${pipeline_args}
}


# # run from the local directory, use local work directory files (-d)
# function local1() {
#     cd "${proj_dir}" || exit 1
#     if [ ! -f secrets/secrets.sh ]; then
#         echo "**** you must check out the secrets too - see the readme">&2; exit 1;
#     fi
#     time bash -c ". secrets/gitlab-deploy/secrets.sh && bin/deploy.sh $pipeline_args --build" || exit 1
#     time bash -c ". secrets/gitlab-deploy/secrets.sh && bin/pipeline.sh $pipeline_args -d" || exit 1
# }
#
# # run from the local directory, reuse existing images
# function local2() {
#     local1
#     cd "${proj_dir}" || exit 1
#     time bash -c ". secrets/gitlab-deploy/secrets.sh && bin/run-confdir.sh $pipeline_args_rundir" || exit 1
# }


###### MAIN

type -t "${1}" >/dev/null || { echo "**** no such test $1" >&2; exit 1; }

"$1"
