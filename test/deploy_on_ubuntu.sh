#!/bin/bash

connect_cmd=""
branch=develop
pipeline_args=""

pass_args=()
while [ "$#" -gt 0 ]; do
    case "$1" in
        --connect)     connect_cmd="$2"; shift;;
        -b|--branch)   branch="$2"; shift;;
        *)             pass_args+=("$1");;
    esac
    shift
done

# restore args
set -- "${pass_args[@]}"

# default to a small run without uploads
pipeline_args="${@:--C pipelines/joblinks -b $branch --arg scraping --light -l 2 --test --all}"

if [[ $connect_cmd =~ ^vagrant ]]; then
    connect_cmd+=" -c"
fi


cat <<"INIT" | $connect_cmd "cat - >init.sh && sudo bash init.sh"
if [ ! -f /usr/bin/pipeline ]; then
        # podman can be fetched from this repo
	. /etc/os-release
	echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /" \
             | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
	curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key \
             | sudo apt-key add -

	apt-get -y update
	apt-get -y install jq curl

        # fuse-overlayfs is supposedly better with podman than VFS - install before installing podman
        curl -L -o /usr/bin/fuse-overlayfs 'https://github.com/containers/fuse-overlayfs/releases/download/v1.1.2/fuse-overlayfs-x86_64'
        chmod a+rx /usr/bin/fuse-overlayfs

        # install podman
	apt-get -y install podman
	ln -s /usr/bin/podman /usr/bin/docker

        # create a tine launch script
	cat << "EOF" | sudo tee /usr/bin/pipeline
#!/bin/bash
if [ -z "$GITLAB_USER" ] || [ -z "$GITLAB_PASS" ] || [ -z "$branch" ]; then echo "**** provide env GITLAB_USER, GITLAB_PASS, branch" >&2; exit 1; fi
curl "https://gitlab.com/joblinks/pipeline/-/raw/$branch/bin/deploy.sh?inline=false" |\
     bash -s -- $@
EOF

        chmod a+rx /usr/bin/pipeline
fi
INIT

if [ -z "$GITLAB_USER" ] || [ -z "$GITLAB_PASS" ]; then
    proj_root="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"/..
    . "${proj_root}"/secrets/gitlab-deploy/secrets.sh || exit 1
fi

export branch

env | grep -E "(GITLAB|branch)" \
    | $connect_cmd "cat - | sed 's|^|export |'> .e && . .e && pipeline $pipeline_args"
