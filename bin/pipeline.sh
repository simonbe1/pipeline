#!/usr/bin/env bash
set -eEu -o pipefail

# Detect the directory name of this script
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
proj_root="${self_dir}/.."

# Load functions
. "${self_dir}/function_lib.sh"

# Make sure we are using a modern Bash
bash_require 4 3 || die "**** please upgrade your Bash version to >= 4.3"


#############################
#### PARSE COMMANDLINE

org_args=($@)

limit="" ; export limit # used in functionlib, fixme as paramter
debug=0
test_mode=0
clean_files=0
from_step=""
to_step=""
skip_steps=""
pipeline_conf="${proj_root}/pipelines/joblinks/pipeline_conf.sh"
create_report=0
declare -A processor_args

while [ "$#" -gt 0 ]; do
    case "$1" in
        --from-step)                        from_step="$2"; clean_files=0; shift;;
        --to-step)                          to_step="$2"; shift;;
        --no|--skip)                        skip_steps+=" $(echo "$2" | tr '-' '_')"; shift;;
        -a|--arg)                           add_arg processor_args "$2" "$3"; shift; shift;;
        -l|--limit)                         limit="$2"; shift;; # truncate input
        -d|--debug) debug=1 ;;              # output to stderr instead of log files
        -k)     clean_files=1 ;;            # clean previous step's files
        -t|--test) test_mode=1 ;;           # indicate that this is a test run
        -c)     pipeline_conf="$2"; shift;; # supply a pipeline configuration file
        *)      ;; # just skip unknown options, until we use getopts
    esac
    shift
done

[[ ! -f "${pipeline_conf}" ]] && { die "**** configuration $pipeline_conf does not exist"; }


#############################
#### CONFIGURATION

# source the pipeline conf
. "$pipeline_conf"
[[ -n "$pipeline" ]] || die "**** '$pipeline_conf' does not define array variable 'pipeline'"

if [ -z "$ldir" ]; then
    ldir=$(mktemp -t "pipeline.XXXXXXXX")
else
    mkdir -p "${ldir}"
fi


#############################
#### INITIAL CHECKS AND OPS

# Check that the necessary tools are installed
for tool in jq curl docker; do
    if ! command -v $tool &> /dev/null; then
        die "**** please install ${tool}"
    fi
done

# Clean last session's files
if [ "$clean_files" = 1 ]; then
    rm -rf "${ldir}"
    mkdir -p "${ldir}"
else
    log "skipping cleaning any existing results"
fi


# remove skipped steps
orig_pipeline=("${pipeline[@]}")
skipped_first=0
filter_pipeline_steps pipeline skipped_first "$from_step" "$to_step" skip_steps

# append command line arguments
add_pipeline_args pipeline processor_args

# create our pipeline command
pipecmd="proc ${debug} ${ldir} $(join_by " | proc ${debug} ${ldir} " pipeline)"

# determine the output file of the first step of the pipeline (the generator, e.g. scraper)
if [ "$skipped_first" = 1 ]; then
    firstlabel=$(echo "${pipeline[0]}" | awk '{print $1}')
    source_program=$(determine_source_program orig_pipeline "${firstlabel}" "${ldir}")
    pipeline_source_output="${ldir}/${source_program}.out"
fi

#############################
#### EXECUTE PIPELINE

log "pipeline to execute:"
for ((i = 0; i < ${#pipeline[@]}; i++)); do
    log "   proc ${pipeline[$i]}"
done

echo "${org_args[*]}" > "${ldir}"/pipeline.cmdline

log "pipeline version "$(git log -1 --pretty="%h %d  %cd  %s")
if [ "$skipped_first" = 1 ]; then
    [ -f "$pipeline_source_output" ] || { die "**** no input data found $pipeline_source_output"; }
    log "pipeline input data: $pipeline_source_output" >&2
    eval "$pipecmd" <"$pipeline_source_output"
else
    eval "$pipecmd" </dev/null
fi
status=$?

## Create a pipeline report with some overall information
if [ -n "${create_report}" ] && [ "${create_report}" = 1 ]; then
    lastlabel=$(echo "${pipeline[-1]}" | awk '{print $1}')
    if [ ! -f "${ldir}"/pipeline.time ] && [ -f "${ldir}"/"${lastlabel}".time ]; then
        cp "${ldir}"/"${lastlabel}".time "${ldir}"/pipeline.time
    fi

    (
        if [ -s "${ldir}"/pipeline.time ]; then
            echo "Pipeline time:"
            sed 's|^|\t\t|' < "${ldir}"/pipeline.time
            echo
        fi
        if [ -s "${ldir}"/pipeline.cmdline ]; then
            echo "Pipeline invocation:"
            sed 's|^|\t\t|' < "${ldir}"/pipeline.cmdline
            echo
        fi
        echo -ne "Pipeline exit status:\n\t\t"
        if [ "${status}" = 0 ]; then
            echo "PASS"
        else
            echo "FAIL"
        fi
        echo
        echo "Pipeline steps:"
        for i in ${!pipeline[@]}; do
            cmdline=(${pipeline[$i]})
            cmdline[1]=${cmdline[1]/$proj_root\//} # tidy up for printing
            echo -e "\t\t${cmdline[@]:1}"
        done
        echo
    ) >"${ldir}"/pipeline.report
fi

exit "${status}"
