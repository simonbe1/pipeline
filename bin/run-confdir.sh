#!/usr/bin/env bash
set -eEu -o pipefail

confdir="${1}" ; shift

self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

[[ ! -d "${confdir}" ]] && { echo "**** directory $confdir does not exist" >&2; exit 1; }

ls -1v "${confdir}"/[0-9][0-9]_* \
    | xargs -L 1 "${self_dir}"/pipeline.sh $@ -c
