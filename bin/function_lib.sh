#!/usr/bin/env bash
set -eEu -o pipefail


function add_arg() {
    local -n hash="$1"
    local name="$2"
    local arg="$3"

    local list=${hash["$name"]:-}
    list+=" $arg"
    hash["$name"]=${list}
}

function is_skipped() {
    local -n slist=$1
    local cmd=$2

    if [ -n "${slist[@]}" ] && [[ " ${slist[@]} " =~ " $cmd " ]]; then
        return 0
    fi
    return 1
}

function log() {
    echo "$(date) pipeline:  $*" >&2
}

function die() {
    log "$@"
    exit 1;
}

function logdest() {
    local debug="$1"; shift
    local logfile="$1"; shift

    if [ "$debug" = 0 ]; then
        $@ 2>>"$logfile"
    else
        $@
    fi
}

function join_by {
    local d="$1"
    local -n f="$2"
    local out=""
    local first=1

    for index in "${!f[@]}" ; do
        local elem=${f[$index]}
        if [ "$first" = 1 ]; then
            out+="$elem"; first=0
        else
            out+="$d $elem"
        fi
    done

    echo "$out"
}

# this is a miniature pipeline processor
function trunc() {
    if [ -n "$limit" ]; then
        jq -c '.[0:'"$limit"']'
    else
        cat -
    fi
}
export -f trunc


# Standard step runner, creating uniform output and log files
function proc() {
    local debug="${1}"; shift
    local ldir="${1}"; shift
    local program="${1}"; shift
    local logfile="${ldir}/${program}".log
    local timefile="${ldir}/${program}".time
    local outfile="${ldir}/${program}".out
    local exe="$1"

    # bash functions cannot be timed, so only time real commands
    local timecmd=
    echo "---- check exe $exe" >&2
    if which "$exe" >/dev/null 2>/dev/null; then
        timecmd="/usr/bin/time -p -o ${timefile}"
    fi

    log "starting processor $program"
    logdest "${debug}" "${logfile}" "${timecmd}" $@ | tee ${outfile} \
        || { echo "***** $program failed!" >&2; \
             test -f "${logfile}" && tail "${logfile}" >&2 ; \
             exit 1; }
}
export -f proc

function determine_source_program() {
    local -n orgpipe="${1}"
    local startelem="${2}"
    local ldir="${3}"

    local lastelem=""
    for index in "${!orgpipe[@]}" ; do
        local all="$(echo ${orgpipe[$index]})"
        local cmd="${all%% *}"

        if [ "$cmd" = "$startelem" ]; then
            break;
        fi

        if [ -s "${ldir}/${cmd}".out ]; then
            lastelem="$cmd"
        fi
    done

    echo "${lastelem}"
}

# remove skipped commands from pipeline
function filter_pipeline_steps() {
    local -n pipel=$1              # is mutated in this procedure
    local -n skipped_datasource=$2 # is mutated in this procedure
    local    from_step=$3
    local    to_step=$4
    local -n steplist=$5           # is mutated in this procedure

    skipped_datasource=0 # first command is the pipeline source, needs special treatment
    for index in "${!pipel[@]}" ; do

        # extract the command label into 'cmd'
        local all="$(echo ${pipel[$index]})"
        local cmd="${all%% *}"

        if [ -n "${from_step}" ] && [ "$from_step" = "$cmd" ]; then
            from_step=""
        fi

        # delete pipeline step if any of the delete conditions are true
        if is_skipped steplist "$cmd" \
               || ( [ -n "${from_step}" ] && [ "$from_step" != "$cmd" ] ) \
               || ( [ -n "${to_step}" ] && [ "${to_step}" = "__delete" ] ); then
            skip_steps+=" $(echo "${pipel[$index]}" | awk '{print $1}' | tr '-' '_')"
            unset -v 'pipel[$index]'
            if [ "$index" = 0 ]; then
                skipped_datasource=1 # mutate
            fi
        fi

        if [ -n "${to_step}" ] && [ "$to_step" = "$cmd" ]; then
            to_step="__delete"
        fi
    done
    pipel=("${pipel[@]}") # mutate
    skip_steps=("${skip_steps[@]}") # mutate
}


function add_pipeline_args() {
    local -n pipel=$1
    local -n args=$2

    for index in "${!pipel[@]}" ; do
        # extract the command label into 'cmd'
        all=$(echo ${pipel[$index]})
        cmd=${all%% *}

        arg="${args[$cmd]:-}"

        if [ -n "$arg" ]; then
            pipel[$index]+="$arg"
        fi
    done
    pipel=("${pipel[@]}")
}


##################################
#### Build and deploy stuff below

function bash_require() {
    local req_maj=$1
    local req_min=$2

    local BV=(${BASH_VERSION//./ })
    if [[ ${BV[0]} -lt ${req_maj} ]] || ( [[ ${BV[0]} = ${req_maj} ]] && [[ ${BV[1]} -lt ${req_min} ]]); then
        return 1
    fi
    return 0
}

function verify_tools() {
    for tool in bash git sed curl docker jq; do
        command -v "${tool}" >/dev/null || { echo "*** please install ${tool}" >&2; exit 1; }
    done

    bash_require 4 3 || { echo "**** upgrade bash to >= 4.3"; exit 1; }
}

function build_processor() {
    local dir="${1}"

    pushd "${dir}" || exit 1

    # the current directory name will do as a unique processor tag
    local label=$(basename $PWD)

    local githash="$(git rev-parse --short HEAD)"

    if ! docker image inspect "${label}":"${githash}" >/dev/null 2>/dev/null; then
        local buildlog="$(mktemp)"

        ./run.sh --build | tee "${buildlog}"


        # podman pattern - put ID on the last line
        local id=$(tail -n 1 "${buildlog}")

        if docker --version|grep -q "^Docker"; then
            # docker pattern - put ID after "Successfully built "
            if grep -q -- '^Successfully built ' "${buildlog}"; then
                id=$(grep -- '^Successfully built ' "${buildlog}" | tail -n 1 | sed 's|^Successfully built ||')
            else
                die "Could not detect ID of built image"
            fi
        fi

        # add a tag 'label:githash', so we can later determine if this
        # git commit's image is already built
        local tag=$(docker image inspect "${id}" | jq -r '.[] | .RepoTags[]' | head -n 1) || exit 1

        docker tag "${tag}" "${label}":"${githash}" || exit 1

        rm -f "${buildlog}"
    else
        log "image tagged "${label}":"${githash}" found - reusing"
    fi
    popd
}

function clean() {
    local proj_root="$1"

    if [ -d "${proj_root}" ]; then
        cd /tmp
        # "${proj_root}"/run.sh --clean    # unnecessary now when we have checks if rebuild is needed
        rm -rf "${proj_root}"
    fi
}
export -f clean

function build() {
    local proj_root="$1"

    if [ -d "${proj_root}" ]; then
        cd "${proj_root}" || exit 1
        local processor_dir=""
        for processor_dir in $(ls -1 "${proj_root}"/processors/*/Dockerfile); do
            build_processor $(dirname "${processor_dir}") || exit 1
        done
    else
        die "**** cannot find proj_root '${proj_root}'"
    fi
}

function run() {
    local proj_root="${1}"; shift

    build "${proj_root}" || exit 1
    cd "${proj_root}"
    bash bin/pipeline.sh $@
}

function run_dir() {
    local proj_root="${1}"; shift
    local confdir="${1}"; shift

    build "${proj_root}" || exit 1
    cd "${proj_root}"
    bash bin/run-confdir.sh "${confdir}" $@
}
