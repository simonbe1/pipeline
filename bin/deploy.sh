#!/usr/bin/env bash
#
# This file is specific to the JobLinks project, and contains
# project specific functions, such as log upload.
#

set -eEu -o pipefail

proj_root="/tmp/pipeline_deploy.$USER"
branch="master"
confdir=""
pipeline_args=("$@")
debug=0

# verify tokens
[[ -n "$GITLAB_USER" ]] || { echo "**** missing setting 'GITLAB_USER'"; exit 1; }
[[ -n "$GITLAB_PASS" ]] || { echo "**** missing setting 'GITLAB_PASS'"; exit 1; }


###############
#### FUNCTIONS

function __read_lib() {
    local proj_root="$1"
    . "${proj_root}/bin/function_lib.sh" || { echo "**** cannot find lib" >&2; exit 1; }
}

function clean_proj() {
    local proj_root="$1"

    if [ -d "${proj_root}" ]; then
        __read_lib "${proj_root}" || exit 1
        clean "${proj_root}" || exit 1
    fi
}

function checkout() {
    local proj_root="$1"

    local dev_dir=$(readlink -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"/..)
    if [ "${debug}" = 1 ] && [ -f "${dev_dir}"/bin/pipeline.sh ]; then
        rsync -az "$dev_dir"/ "${proj_root}"
    else
        if [ -d "${proj_root}"/.git ]; then
            cd "${proj_root}"
            git pull || exit 1
            git submodule update --recursive --remote || exit 1
        else
            git clone -b "${branch}" https://gitlab.com/joblinks/pipeline.git "${proj_root}" || exit 1
            cd "${proj_root}"
            git clone -b "${branch}" "https://$GITLAB_USER:$GITLAB_PASS@gitlab.com/joblinks/secrets.git" || exit 1
            sed -i "s|gitlab.com|$GITLAB_USER:$GITLAB_PASS@gitlab.com|" .gitmodules
            git submodule update --init --recursive 2>&1 | sed 's|https://.*gitlab.com|https://gitlab.com|' || exit 1
            chmod -R go= "${proj_root}"
        fi
    fi
}

function build_proj() {
    local proj_root="$1"

    checkout "${proj_root}" || exit 1

    __read_lib "${proj_root}" || exit 1

    build "${proj_root}" || exit 1
}

function all() {
    local proj_root="${1}"; shift

    clean_proj "${proj_root}" || exit 1
    checkout "${proj_root}" || exit 1
    build_proj "${proj_root}" || exit 1
    run_dir "${proj_root}" "${confdir}" $@ || exit 1
    #clean_proj "${proj_root}"
}

###############
#### MAIN

pass_args=()
while [ "$#" -gt 0 ]; do
    case "$1" in
        -b)            branch="$2"; shift;;
        -C)            confdir="$2"; shift;;
        -d)            pass_args+=("$1"); debug=1;;
        *)             pass_args+=("$1");;
    esac
    shift
done

# restore args
set -- "${pass_args[@]}"


if [ -f "${proj_root}"/bin/function_lib.sh ]; then
    . "${proj_root}"/bin/function_lib.sh
fi

# act on selected operation
while [ "$#" -gt 0 ]; do
    case "$1" in
        --clean)       clean_proj "${proj_root}";;
        --checkout)    checkout "${proj_root}";;
        --build)       build_proj "${proj_root}";;
        --run)         checkout "${proj_root}" && . "${proj_root}"/bin/function_lib.sh && run_dir "${proj_root}" "${confdir}" ${pipeline_args[@]};;
        --all)         all "${proj_root}" ${pipeline_args[@]};;
        *)             true;;
    esac
    shift
done
